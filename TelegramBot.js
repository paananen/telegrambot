var https = require("https");
var urlHelper = require("./url-helper");
var querystring = require('querystring');

function TelegramBot(token) {
	global.debug = process.argv[2] == 'debug';
	this.token = token;
	this.lastUpdateID = 0;
	this.httpsOptions = {
		hostname: "api.telegram.org",
		port: "443",
		method: "POST"
	};
}

TelegramBot.prototype.sendRequest = function (command, parameters, responseHandler) {
	var postData = querystring.stringify(parameters);
	this.httpsOptions.path = this.generatePath(this.token, command);
	this.httpsOptions.headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Content-Length': Buffer.byteLength(postData)
	}
	if(global.debug) console.log(this.httpsOptions.path);
	var request = https.request(this.httpsOptions, responseHandler);
	request.on('error', function (err) {
		console.log(err);
	});
	if(global.debug) console.log(postData);
	request.write(postData);
	request.end();
}

TelegramBot.prototype.setTyping = function (chatId) {
	this.sendRequest('sendChatAction', {chat_id: chatId, action: 'typing'});
}

TelegramBot.prototype.generatePath = function (token, command) {
	return "/bot" + token + "/" + command;
}

TelegramBot.prototype.getUpdates = function (updatesHandler) {
	var that = this;
	var parameters = {
		offset : this.lastUpdateID + 1,
		limit : 10
	};
	try {
		this.sendRequest("getUpdates", parameters, function (response) {
			let responseData = '';
			response.on("data", function (data) {
				responseData += data;
			});
			response.on("end", function () {
				if(global.debug) console.log(responseData.toString("utf8"));
				try {
					var parsedData = JSON.parse(responseData);
				} catch(ex) {
					console.log(ex);
				}
				if(typeof parsedData == 'undefined') {
					return;
				}
				var result = parsedData.result;
				if(global.debug) console.log(result);
				if(typeof result == 'undefined') {
					return;
				}
				if(result.length > 0) {
					that.lastUpdateID = result[result.length - 1].update_id;
				}
				updatesHandler(result);
			});
		});
	} catch (ex) {
		console.log(ex);
	}
}

TelegramBot.prototype.sendMessage = function (text, chatID, disableWebPagePreview = false, responseMessageId = null) {
	var parameters = {
		chat_id: chatID,
		text: text,
		parse_mode: 'HTML',
		disable_web_page_preview: disableWebPagePreview,
		reply_to_message_id: responseMessageId
	};
	this.sendRequest("sendMessage", parameters, function (response) {
		response.on("data", function (data) {
			if(global.debug) console.log(JSON.parse(data));
		});
	});
}

module.exports = TelegramBot;
